package io.swagger;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@ComponentScan(basePackages = { 
  "io.swagger",
  "io.swagger.configuration",
  "com.fibapix.covid_tracker.core.api" , 
  "com.fibapix.covid_tracker.core.controller",
  "com.fibapix.covid_tracker.core"
})
@EnableCaching
@EnableAsync
@EnableScheduling
@EnableAutoConfiguration(exclude= {HibernateJpaAutoConfiguration.class})
@SpringBootApplication
@EnableSwagger2
public class SwaggerCovidTrackerCoreApi implements CommandLineRunner {

    @Override
    public void run(String... arg0) throws Exception {
        if (arg0.length > 0 && arg0[0].equals("exitcode")) {
            throw new ExitException();
        }
    }

    public static void main(String[] args) throws Exception {
        new SpringApplication(SwaggerCovidTrackerCoreApi.class).run(args);
    }

    class ExitException extends RuntimeException implements ExitCodeGenerator {
        private static final long serialVersionUID = 1L;

        @Override
        public int getExitCode() {
            return 10;
        }

    }
}
