package com.fibapix.covid_tracker.core.service.covidtest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fibapix.covid_tracker.core.commons.domain.covidtest.TCovidTest;
import com.fibapix.covid_tracker.core.commons.type.covidtest.CovidTestListType;
import com.fibapix.covid_tracker.core.commons.type.covidtest.CovidTestType;
import com.fibapix.covid_tracker.core.converter.covidtest.CovidTestConverter;
import com.fibapix.covid_tracker.core.dao.covidtest.ICovidTestDAO;

@Service
public class CovidTestService implements ICovidTestService {
	
	private final ICovidTestDAO dao;

	@Autowired
	public CovidTestService(ICovidTestDAO dao) {
		this.dao = dao;
	}
	
	@Override
	@Transactional
	public CovidTestType saveCovidTest(CovidTestType covidTest) {
		try {
			TCovidTest tCovidTest = CovidTestConverter.convertTypeToEntity(covidTest);
			dao.save(tCovidTest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return covidTest;
	}

	@Override
	@Transactional
	public CovidTestType updateCovidTest(CovidTestType covidTest) {
		try {
			TCovidTest tCovidTest = CovidTestConverter.convertTypeToEntity(covidTest);
			dao.update(tCovidTest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return covidTest;
	}

	@Override
	@Transactional
	public CovidTestListType getAllCovidTests() {
		CovidTestListType covidTestLstType = new CovidTestListType();
		try {
			List<TCovidTest> lstTCovidTest = dao.getAll();
			covidTestLstType.setCovidTests(CovidTestConverter.convertListEntityToType(lstTCovidTest));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return covidTestLstType;
	}

	@Override
	@Transactional
	public CovidTestType getCovidTestById(Long id) {
		CovidTestType covidTestType = new CovidTestType();
		try {
			TCovidTest tCovidTest = dao.getCovidTestById(id);
			covidTestType = CovidTestConverter.convertEntityToType(tCovidTest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return covidTestType;
	}

	@Override
	@Transactional
	public void disableCovidTestById(Long id) {
		try {
			dao.disable(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
