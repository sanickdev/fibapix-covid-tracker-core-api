package com.fibapix.covid_tracker.core.service.catalog.store;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fibapix.covid_tracker.core.commons.domain.catalog.store.TStore;
import com.fibapix.covid_tracker.core.commons.type.catalog.store.StoreListType;
import com.fibapix.covid_tracker.core.commons.type.catalog.store.StoreType;
import com.fibapix.covid_tracker.core.converter.catalog.store.StoreConverter;
import com.fibapix.covid_tracker.core.dao.catalog.store.IStoreDAO;

@Service
public class StoreService implements IStoreService {
	
	private final IStoreDAO dao;
	
	@Autowired
	public StoreService(IStoreDAO dao) {
		this.dao = dao;
	}

	@Override
	@Transactional
	public StoreType saveStore(StoreType storeType) {
		try {
			TStore store = StoreConverter.convertTypeToEntity(storeType);
			dao.save(store);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return storeType;
	}

	@Override
	@Transactional
	public StoreType updateStore(StoreType storeType) {
		try {
			TStore store = StoreConverter.convertTypeToEntity(storeType);
			dao.update(store);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return storeType;
	}

	@Override
	@Transactional
	public StoreListType getAllStores() {
		StoreListType storeListType = new StoreListType();
		try {
			List<TStore> lstStore = dao.getAll();
			List<StoreType> lstStoreType = StoreConverter.convertListEntityToType(lstStore);
			storeListType.setStores(lstStoreType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return storeListType;
	}

	@Override
	@Transactional
	public StoreType getStoreById(Long id) {
		StoreType storeType = new StoreType();
		try {
			TStore store = dao.getStoreById(id);
			storeType = StoreConverter.convertEntityToType(store);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return storeType;
	}

	@Override
	@Transactional
	public void disableStore(Long id) {
		try {
			dao.disable(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
