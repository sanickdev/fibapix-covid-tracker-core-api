package com.fibapix.covid_tracker.core.service.catalog.store;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fibapix.covid_tracker.core.dao.catalog.store.IPaycomStoreDAO;

@Service
public class PaycomStoreService implements IPaycomStoreService {
	
	private final IPaycomStoreDAO dao;
	
	@Autowired
	public PaycomStoreService(IPaycomStoreDAO dao) {
		this.dao = dao;
	}

	@Override
	@Transactional
	public List<?> getJobs() {
		try {
			dao.getJobs();
		} catch (Exception e) {
			// TODO: handle exception
		}
		// TODO Auto-generated method stub
		return null;
	}

}
