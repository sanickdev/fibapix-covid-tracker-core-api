package com.fibapix.covid_tracker.core.service.security;

import com.fibapix.covid_tracker.core.commons.type.security.OperSecurityAllType;
import com.fibapix.covid_tracker.core.commons.type.security.UserType;

public interface ISecurityService {
	
	public OperSecurityAllType login(UserType userType);

}
