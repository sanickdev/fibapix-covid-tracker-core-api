package com.fibapix.covid_tracker.core.service.catalog.store;

import com.fibapix.covid_tracker.core.commons.type.catalog.store.StoreListType;
import com.fibapix.covid_tracker.core.commons.type.catalog.store.StoreType;

public interface IStoreService {
	
	public StoreType saveStore(StoreType storeType);
	public StoreType updateStore(StoreType storeType);
	public StoreListType getAllStores();
	public StoreType getStoreById(Long id);
	public void disableStore(Long id);

}
