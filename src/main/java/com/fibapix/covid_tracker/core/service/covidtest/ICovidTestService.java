package com.fibapix.covid_tracker.core.service.covidtest;

import com.fibapix.covid_tracker.core.commons.type.covidtest.CovidTestListType;
import com.fibapix.covid_tracker.core.commons.type.covidtest.CovidTestType;

public interface ICovidTestService {
	
	public CovidTestType saveCovidTest(CovidTestType covidTest);
	public CovidTestType updateCovidTest(CovidTestType covidTest);
	public CovidTestListType getAllCovidTests();
	public CovidTestType getCovidTestById(Long id);
	public void disableCovidTestById(Long id);

}
