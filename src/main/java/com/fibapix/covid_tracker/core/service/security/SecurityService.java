package com.fibapix.covid_tracker.core.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fibapix.covid_tracker.core.commons.type.security.OperSecurityAllType;
import com.fibapix.covid_tracker.core.commons.type.security.UserType;
import com.fibapix.covid_tracker.core.dao.security.IPaycomSecurityDAO;

@Service
public class SecurityService implements ISecurityService {
	
	private final IPaycomSecurityDAO dao;
	
	@Autowired
	public SecurityService(IPaycomSecurityDAO dao) {
		this.dao = dao;
	}

	@Override
	public OperSecurityAllType login(UserType userType) {
		OperSecurityAllType operSecurityType = new OperSecurityAllType();
		operSecurityType = dao.login(userType);
		return operSecurityType;
	}

	

}
