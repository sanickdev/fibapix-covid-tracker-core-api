package com.fibapix.covid_tracker.core.controller.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fibapix.covid_tracker.core.api.LoginApi;
import com.fibapix.covid_tracker.core.commons.type.security.OperSecurityAllType;
import com.fibapix.covid_tracker.core.filter.security.SecurityFilter;
import com.fibapix.covid_tracker.core.service.security.ISecurityService;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-01-04T17:57:34.332-06:00[America/Mexico_City]")
@Controller
public class LoginApiController implements LoginApi {

    private static final Logger log = LoggerFactory.getLogger(LoginApiController.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    private final ISecurityService securityService;

    @org.springframework.beans.factory.annotation.Autowired
    public LoginApiController(ObjectMapper objectMapper, HttpServletRequest request, ISecurityService securityService) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.securityService = securityService;
    }

    public ResponseEntity<OperSecurityAllType> login(@ApiParam(value = "Return a User object" ,required=true )  @Valid @RequestBody OperSecurityAllType body) {
    	
    	OperSecurityAllType operSecurityAllType = new OperSecurityAllType();
    	try {
    		String filter = SecurityFilter.validateLoginRequest(body.getSecurity());
    		operSecurityAllType = securityService.login(body.getSecurity());
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return ResponseEntity.ok().body(operSecurityAllType);
    }


}
