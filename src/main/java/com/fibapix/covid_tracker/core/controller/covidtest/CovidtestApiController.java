package com.fibapix.covid_tracker.core.controller.covidtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fibapix.covid_tracker.core.api.CovidtestApi;
import com.fibapix.covid_tracker.core.commons.type.covidtest.CovidTestListType;
import com.fibapix.covid_tracker.core.commons.type.covidtest.CovidTestType;
import com.fibapix.covid_tracker.core.commons.type.covidtest.OperCovidTestAllType;
import com.fibapix.covid_tracker.core.commons.type.covidtest.OperCovidTestListAllType;
import com.fibapix.covid_tracker.core.filter.covidtest.CovidTestFilter;
import com.fibapix.covid_tracker.core.service.covidtest.ICovidTestService;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
@Controller
public class CovidtestApiController implements CovidtestApi {

    private static final Logger log = LoggerFactory.getLogger(CovidtestApiController.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    private final ICovidTestService covidTestService;

    @org.springframework.beans.factory.annotation.Autowired
    public CovidtestApiController(ObjectMapper objectMapper, HttpServletRequest request, ICovidTestService covidTestService) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.covidTestService = covidTestService;
    }

    public ResponseEntity<OperCovidTestListAllType> getAll(
    		@ApiParam(value = "CovidTest by ID") @Valid @RequestParam(value = "id", required = false) Long id) {
    	
        OperCovidTestListAllType operCovidTestListAllType = new OperCovidTestListAllType();
		try {
			CovidTestListType covidTestListType = covidTestService.getAllCovidTests();
			operCovidTestListAllType.setCovidtests(covidTestListType);
		} catch (Exception e) {
			
		}
		return ResponseEntity.ok().body(operCovidTestListAllType);
    }

    public ResponseEntity<OperCovidTestAllType> save(
    		@ApiParam(value = "Insert a CovidTest object." ,required=true )  @Valid @RequestBody OperCovidTestAllType body) {
    	OperCovidTestAllType operCovidTestAllType = new OperCovidTestAllType();
		try {
			 String filter = CovidTestFilter.validateRequest(body.getCovidtest());
			 CovidTestType covidTestType = covidTestService.saveCovidTest(body.getCovidtest());
			 operCovidTestAllType.setCovidtest(covidTestType);
		} catch (Exception e) {
			
		} finally {
			
		}
		return ResponseEntity.ok().body(operCovidTestAllType);
    }

    public ResponseEntity<OperCovidTestAllType> update(
    		@ApiParam(value = "Update a CovidTest object." ,required=true )  @Valid @RequestBody OperCovidTestAllType body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperCovidTestAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

}
