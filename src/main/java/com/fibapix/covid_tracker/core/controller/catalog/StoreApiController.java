package com.fibapix.covid_tracker.core.controller.catalog;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fibapix.covid_tracker.core.api.StoreApi;
import com.fibapix.covid_tracker.core.commons.type.catalog.store.OperStoreAllType;
import com.fibapix.covid_tracker.core.commons.type.catalog.store.OperStoreListAllType;
import com.fibapix.covid_tracker.core.commons.type.catalog.store.StoreListType;
import com.fibapix.covid_tracker.core.commons.type.catalog.store.StoreType;
import com.fibapix.covid_tracker.core.filter.catalog.store.StoreFilter;
import com.fibapix.covid_tracker.core.service.catalog.store.IStoreService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
@Controller
public class StoreApiController implements StoreApi {

    private static final Logger log = LoggerFactory.getLogger(StoreApiController.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    private final IStoreService storeService;

    @org.springframework.beans.factory.annotation.Autowired
    public StoreApiController(ObjectMapper objectMapper, HttpServletRequest request, IStoreService storeService) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.storeService = storeService;
    }

    public ResponseEntity<Void> disable(@ApiParam(value = "Territory ID",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<OperStoreListAllType> getAll(
    		@ApiParam(value = "Store by ID") @Valid @RequestParam(value = "id", required = false) Long id,
    		@ApiParam(value = "Stores by name") @Valid @RequestParam(value = "name", required = false) String name) {
        OperStoreListAllType listAll = new OperStoreListAllType();
        try {
        	StoreListType storeListType = storeService.getAllStores();
        	listAll.setStores(storeListType);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return ResponseEntity.ok().body(listAll);
    }

    public ResponseEntity<OperStoreAllType> getStoreById(@ApiParam(value = "Store ID",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperStoreAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<OperStoreAllType> save(@ApiParam(value = "Insert a store object." ,required=true )  @Valid @RequestBody OperStoreAllType body) {
        OperStoreAllType operStore = new OperStoreAllType();
    	try {
			String filter = StoreFilter.validateRequest(body.getStore());
			StoreType storeType = storeService.saveStore(body.getStore());
			operStore.setStore(storeType);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return ResponseEntity.ok().body(operStore);
    }

    public ResponseEntity<OperStoreAllType> update(@ApiParam(value = "Update a store object." ,required=true )  @Valid @RequestBody OperStoreAllType body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperStoreAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

}
