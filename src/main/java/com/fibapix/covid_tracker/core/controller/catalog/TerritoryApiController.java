package com.fibapix.covid_tracker.core.controller.catalog;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fibapix.covid_tracker.core.api.TerritoryApi;
import com.fibapix.covid_tracker.core.commons.type.catalog.territory.OperTerritoryAllType;
import com.fibapix.covid_tracker.core.commons.type.catalog.territory.OperTerritoryListAllType;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
@Controller
public class TerritoryApiController implements TerritoryApi {

    private static final Logger log = LoggerFactory.getLogger(TerritoryApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public TerritoryApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> disable(@ApiParam(value = "Territory ID",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<OperTerritoryListAllType> getAll(@ApiParam(value = "Territory by ID") @Valid @RequestParam(value = "id", required = false) Long id,@ApiParam(value = "Territories by name") @Valid @RequestParam(value = "name", required = false) String name) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperTerritoryListAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<OperTerritoryAllType> getTerritoryById(@ApiParam(value = "Territory ID",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperTerritoryAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<OperTerritoryAllType> save(@ApiParam(value = "Insert a territory object." ,required=true )  @Valid @RequestBody OperTerritoryAllType body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperTerritoryAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<OperTerritoryAllType> update(@ApiParam(value = "Update a territory object." ,required=true )  @Valid @RequestBody OperTerritoryAllType body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperTerritoryAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

}
