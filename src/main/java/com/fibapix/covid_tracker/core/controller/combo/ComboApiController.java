package com.fibapix.covid_tracker.core.controller.combo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fibapix.covid_tracker.core.api.ComboApi;
import com.fibapix.covid_tracker.core.commons.type.combo.OperComboListAllType;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
@Controller
public class ComboApiController implements ComboApi {

    private static final Logger log = LoggerFactory.getLogger(ComboApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public ComboApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<OperComboListAllType> getCombo(@ApiParam(value = "Combo",required=true) @PathVariable("option") String option) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperComboListAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

}
