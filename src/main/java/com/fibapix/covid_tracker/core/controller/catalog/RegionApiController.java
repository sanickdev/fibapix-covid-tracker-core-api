package com.fibapix.covid_tracker.core.controller.catalog;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fibapix.covid_tracker.core.api.RegionApi;
import com.fibapix.covid_tracker.core.commons.type.catalog.region.OperRegionAllType;
import com.fibapix.covid_tracker.core.commons.type.catalog.region.OperRegionListAllType;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
@Controller
public class RegionApiController implements RegionApi {

    private static final Logger log = LoggerFactory.getLogger(RegionApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public RegionApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> disable(@ApiParam(value = "Region ID",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<OperRegionListAllType> getAll(@ApiParam(value = "Region by ID") @Valid @RequestParam(value = "id", required = false) Long id,@ApiParam(value = "Regions by name") @Valid @RequestParam(value = "name", required = false) String name) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperRegionListAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<OperRegionAllType> getRegionById(@ApiParam(value = "Region ID",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperRegionAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<OperRegionAllType> save(@ApiParam(value = "Insert a region object." ,required=true )  @Valid @RequestBody OperRegionAllType body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperRegionAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<OperRegionAllType> update(@ApiParam(value = "Update a region object." ,required=true )  @Valid @RequestBody OperRegionAllType body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<OperRegionAllType>(HttpStatus.NOT_IMPLEMENTED);
    }

}
