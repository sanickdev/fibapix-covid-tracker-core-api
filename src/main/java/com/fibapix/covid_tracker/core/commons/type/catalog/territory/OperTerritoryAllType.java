package com.fibapix.covid_tracker.core.commons.type.catalog.territory;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * OperTerritoryAllType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
public class OperTerritoryAllType   {
  @JsonProperty("territory")
  private TerritoryType territory = null;

  public OperTerritoryAllType territory(TerritoryType territory) {
    this.territory = territory;
    return this;
  }

  /**
   * Get territory
   * @return territory
  **/
  @ApiModelProperty(value = "")

  @Valid
  public TerritoryType getTerritory() {
    return territory;
  }

  public void setTerritory(TerritoryType territory) {
    this.territory = territory;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OperTerritoryAllType operTerritoryAllType = (OperTerritoryAllType) o;
    return Objects.equals(this.territory, operTerritoryAllType.territory);
  }

  @Override
  public int hashCode() {
    return Objects.hash(territory);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OperTerritoryAllType {\n");
    
    sb.append("    territory: ").append(toIndentedString(territory)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
