package com.fibapix.covid_tracker.core.commons.type.security;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * User
 */
@ApiModel(description = "User")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-01-04T22:42:00.943-06:00[America/Mexico_City]")
public class UserType {
	@JsonProperty("id")
	private Long id = null;

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("lastname")
	private String lastname = null;

	@JsonProperty("username")
	private String username = null;

	@JsonProperty("password")
	private String password = null;

	@JsonProperty("socialSecurityNo")
	private String socialSecurityNo = null;

	@JsonProperty("employeeNo")
	private String employeeNo = null;

	public UserType id(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id
	 * 
	 * @return id
	 **/
	@ApiModelProperty(value = "")

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserType name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Get name
	 * 
	 * @return name
	 **/
	@ApiModelProperty(value = "")

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserType lastname(String lastname) {
		this.lastname = lastname;
		return this;
	}

	/**
	 * Get lastname
	 * 
	 * @return lastname
	 **/
	@ApiModelProperty(value = "")

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public UserType username(String username) {
		this.username = username;
		return this;
	}

	/**
	 * Get username
	 * 
	 * @return username
	 **/
	@ApiModelProperty(value = "")

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserType password(String password) {
		this.password = password;
		return this;
	}

	/**
	 * Get password
	 * 
	 * @return password
	 **/
	@ApiModelProperty(value = "")

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserType socialSecurityNo(String socialSecurityNo) {
		this.socialSecurityNo = socialSecurityNo;
		return this;
	}

	/**
	 * Get socialSecurityNo
	 * 
	 * @return socialSecurityNo
	 **/
	@ApiModelProperty(value = "")

	public String getSocialSecurityNo() {
		return socialSecurityNo;
	}

	public void setSocialSecurityNo(String socialSecurityNo) {
		this.socialSecurityNo = socialSecurityNo;
	}

	public UserType employeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
		return this;
	}

	/**
	 * Get employeeNo
	 * 
	 * @return employeeNo
	 **/
	@ApiModelProperty(value = "")

	public String getEmployeeNo() {
		return employeeNo;
	}

	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		UserType userType = (UserType) o;
		return Objects.equals(this.id, userType.id) && Objects.equals(this.name, userType.name)
				&& Objects.equals(this.lastname, userType.lastname) && Objects.equals(this.username, userType.username)
				&& Objects.equals(this.password, userType.password)
				&& Objects.equals(this.socialSecurityNo, userType.socialSecurityNo)
				&& Objects.equals(this.employeeNo, userType.employeeNo);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, lastname, username, password, socialSecurityNo, employeeNo);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class UserType {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    lastname: ").append(toIndentedString(lastname)).append("\n");
		sb.append("    username: ").append(toIndentedString(username)).append("\n");
		sb.append("    password: ").append(toIndentedString(password)).append("\n");
		sb.append("    socialSecurityNo: ").append(toIndentedString(socialSecurityNo)).append("\n");
		sb.append("    employeeNo: ").append(toIndentedString(employeeNo)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
