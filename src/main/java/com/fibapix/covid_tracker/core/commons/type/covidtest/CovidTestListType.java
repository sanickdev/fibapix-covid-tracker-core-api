package com.fibapix.covid_tracker.core.commons.type.covidtest;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * Collection of CovidTestType
 */
@ApiModel(description = "Collection of CovidTestType")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
public class CovidTestListType   {
  @JsonProperty("covidTests")
  @Valid
  private List<CovidTestType> covidTests = null;

  public CovidTestListType covidTests(List<CovidTestType> covidTests) {
    this.covidTests = covidTests;
    return this;
  }

  public CovidTestListType addCovidTestsItem(CovidTestType covidTestsItem) {
    if (this.covidTests == null) {
      this.covidTests = new ArrayList<CovidTestType>();
    }
    this.covidTests.add(covidTestsItem);
    return this;
  }

  /**
   * Get covidTests
   * @return covidTests
  **/
  @ApiModelProperty(value = "")
  @Valid
  public List<CovidTestType> getCovidTests() {
    return covidTests;
  }

  public void setCovidTests(List<CovidTestType> covidTests) {
    this.covidTests = covidTests;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CovidTestListType covidTestListType = (CovidTestListType) o;
    return Objects.equals(this.covidTests, covidTestListType.covidTests);
  }

  @Override
  public int hashCode() {
    return Objects.hash(covidTests);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CovidTestListType {\n");
    
    sb.append("    covidTests: ").append(toIndentedString(covidTests)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
