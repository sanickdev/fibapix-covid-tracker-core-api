package com.fibapix.covid_tracker.core.commons.domain.covidtest;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;
import org.hibernate.envers.Audited;
import com.fibapix.covid_tracker.core.commons.domain.general.TAudit;
import com.fibapix.covid_tracker.core.commons.domain.general.TImage;
import org.codehaus.jackson.annotate.JsonIgnore;
import javax.persistence.ForeignKey;

@Entity
@Table(name = "TCovidTest")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
@Audited(targetAuditMode = org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED)
@OptimisticLocking(type = OptimisticLockType.VERSION)
public class TCovidTest extends TAudit implements java.io.Serializable {

	private static final long serialVersionUID = -6449038185718511016L;
	private Long id;
	private String name;
	private Date recordCovidTestDate;
	private boolean isPositive;
	private TImage photo;
	private boolean status;

	public TCovidTest(Long id, String name, Date recordCovidTestDate, boolean isPositive, TImage photo,
			boolean status) {
		super();
		this.id = id;
		this.name = name;
		this.recordCovidTestDate = recordCovidTestDate;
		this.isPositive = isPositive;
		this.photo = photo;
		this.status = status;
	}

	public TCovidTest() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 30)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "recordCovidTestDate", nullable = true, updatable = false, columnDefinition = "datetime")
	public Date getRecordCovidTestDate() {
		return recordCovidTestDate;
	}

	public void setRecordCovidTestDate(Date recordCovidTestDate) {
		this.recordCovidTestDate = recordCovidTestDate;
	}
	
	@Column(name = "isPositive", columnDefinition = "bit default 1")
	public boolean isPositive() {
		return isPositive;
	}

	public void setPositive(boolean isPositive) {
		this.isPositive = isPositive;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_photo", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_photo"))
	public TImage getPhoto() {
		return photo;
	}

	public void setPhoto(TImage photo) {
		this.photo = photo;
	}

	@Column(name = "status", columnDefinition = "bit default 1")
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
