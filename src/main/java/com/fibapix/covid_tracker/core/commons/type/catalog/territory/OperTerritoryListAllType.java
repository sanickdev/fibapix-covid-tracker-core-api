package com.fibapix.covid_tracker.core.commons.type.catalog.territory;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * OperTerritoryListAllType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
public class OperTerritoryListAllType   {
  @JsonProperty("territories")
  private TerritoryListType territories = null;

  public OperTerritoryListAllType territories(TerritoryListType territories) {
    this.territories = territories;
    return this;
  }

  /**
   * Get territories
   * @return territories
  **/
  @ApiModelProperty(value = "")

  @Valid
  public TerritoryListType getTerritories() {
    return territories;
  }

  public void setTerritories(TerritoryListType territories) {
    this.territories = territories;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OperTerritoryListAllType operTerritoryListAllType = (OperTerritoryListAllType) o;
    return Objects.equals(this.territories, operTerritoryListAllType.territories);
  }

  @Override
  public int hashCode() {
    return Objects.hash(territories);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OperTerritoryListAllType {\n");
    
    sb.append("    territories: ").append(toIndentedString(territories)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
