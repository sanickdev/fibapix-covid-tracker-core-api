package com.fibapix.covid_tracker.core.commons.type.catalog.store;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * OperStoreListAllType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
public class OperStoreListAllType   {
  @JsonProperty("stores")
  private StoreListType stores = null;

  public OperStoreListAllType stores(StoreListType stores) {
    this.stores = stores;
    return this;
  }

  /**
   * Get stores
   * @return stores
  **/
  @ApiModelProperty(value = "")

  @Valid
  public StoreListType getStores() {
    return stores;
  }

  public void setStores(StoreListType stores) {
    this.stores = stores;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OperStoreListAllType operStoreListAllType = (OperStoreListAllType) o;
    return Objects.equals(this.stores, operStoreListAllType.stores);
  }

  @Override
  public int hashCode() {
    return Objects.hash(stores);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OperStoreListAllType {\n");
    
    sb.append("    stores: ").append(toIndentedString(stores)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
