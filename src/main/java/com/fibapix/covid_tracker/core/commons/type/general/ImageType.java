package com.fibapix.covid_tracker.core.commons.type.general;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * ImageType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-22T15:07:35.551-06:00[America/Mexico_City]")
public class ImageType {
	@JsonProperty("id")
	private Long id = null;

	@JsonProperty("base64")
	private String base64 = null;

	public ImageType id(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id
	 * 
	 * @return id
	 **/
	@ApiModelProperty(value = "")

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ImageType base64(String base64) {
		this.base64 = base64;
		return this;
	}

	/**
	 * Get base64
	 * 
	 * @return base64
	 **/
	@ApiModelProperty(value = "")

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ImageType imageType = (ImageType) o;
		return Objects.equals(this.id, imageType.id) && Objects.equals(this.base64, imageType.base64);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, base64);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ImageType {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    base64: ").append(toIndentedString(base64)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
