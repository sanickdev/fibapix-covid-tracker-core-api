package com.fibapix.covid_tracker.core.commons.type.covidtest;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * OperCovidTestListAllType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
public class OperCovidTestListAllType   {
  @JsonProperty("covidtests")
  private CovidTestListType covidtests = null;

  public OperCovidTestListAllType covidtests(CovidTestListType covidtests) {
    this.covidtests = covidtests;
    return this;
  }

  /**
   * Get covidtests
   * @return covidtests
  **/
  @ApiModelProperty(value = "")

  @Valid
  public CovidTestListType getCovidtests() {
    return covidtests;
  }

  public void setCovidtests(CovidTestListType covidtests) {
    this.covidtests = covidtests;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OperCovidTestListAllType operCovidTestListAllType = (OperCovidTestListAllType) o;
    return Objects.equals(this.covidtests, operCovidTestListAllType.covidtests);
  }

  @Override
  public int hashCode() {
    return Objects.hash(covidtests);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OperCovidTestListAllType {\n");
    
    sb.append("    covidtests: ").append(toIndentedString(covidtests)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
