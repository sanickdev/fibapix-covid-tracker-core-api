package com.fibapix.covid_tracker.core.commons.type.catalog.territory;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * Collection of TerritoryType
 */
@ApiModel(description = "Collection of TerritoryType")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
public class TerritoryListType   {
  @JsonProperty("territories")
  @Valid
  private List<TerritoryType> territories = null;

  public TerritoryListType territories(List<TerritoryType> territories) {
    this.territories = territories;
    return this;
  }

  public TerritoryListType addTerritoriesItem(TerritoryType territoriesItem) {
    if (this.territories == null) {
      this.territories = new ArrayList<TerritoryType>();
    }
    this.territories.add(territoriesItem);
    return this;
  }

  /**
   * Get territories
   * @return territories
  **/
  @ApiModelProperty(value = "")
  @Valid
  public List<TerritoryType> getTerritories() {
    return territories;
  }

  public void setTerritories(List<TerritoryType> territories) {
    this.territories = territories;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TerritoryListType territoryListType = (TerritoryListType) o;
    return Objects.equals(this.territories, territoryListType.territories);
  }

  @Override
  public int hashCode() {
    return Objects.hash(territories);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TerritoryListType {\n");
    
    sb.append("    territories: ").append(toIndentedString(territories)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
