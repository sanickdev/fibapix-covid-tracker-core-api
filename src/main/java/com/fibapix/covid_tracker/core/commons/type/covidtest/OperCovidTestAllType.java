package com.fibapix.covid_tracker.core.commons.type.covidtest;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * OperCovidTestAllType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
public class OperCovidTestAllType   {
  @JsonProperty("covidtest")
  private CovidTestType covidtest = null;

  public OperCovidTestAllType covidtest(CovidTestType covidtest) {
    this.covidtest = covidtest;
    return this;
  }

  /**
   * Get covidtest
   * @return covidtest
  **/
  @ApiModelProperty(value = "")

  @Valid
  public CovidTestType getCovidtest() {
    return covidtest;
  }

  public void setCovidtest(CovidTestType covidtest) {
    this.covidtest = covidtest;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OperCovidTestAllType operCovidTestAllType = (OperCovidTestAllType) o;
    return Objects.equals(this.covidtest, operCovidTestAllType.covidtest);
  }

  @Override
  public int hashCode() {
    return Objects.hash(covidtest);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OperCovidTestAllType {\n");
    
    sb.append("    covidtest: ").append(toIndentedString(covidtest)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
