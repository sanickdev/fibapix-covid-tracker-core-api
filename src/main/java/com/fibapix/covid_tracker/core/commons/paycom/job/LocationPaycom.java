package com.fibapix.covid_tracker.core.commons.paycom.job;

public class LocationPaycom {

	private String locid;
	private String description;
	private String stateFullName;

	public LocationPaycom() {
		super();
	}

	public LocationPaycom(String locid, String description, String stateFullName) {
		super();
		this.locid = locid;
		this.description = description;
		this.stateFullName = stateFullName;
	}

	public String getLocid() {
		return locid;
	}

	public void setLocid(String locid) {
		this.locid = locid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStateFullName() {
		return stateFullName;
	}

	public void setStateFullName(String stateFullName) {
		this.stateFullName = stateFullName;
	}

}
