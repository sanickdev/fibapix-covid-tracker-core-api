package com.fibapix.covid_tracker.core.commons.type.security;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * OperSecurityAllType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-01-04T23:11:04.636-06:00[America/Mexico_City]")
public class OperSecurityAllType {
	@JsonProperty("security")
	private UserType security = null;

	@JsonProperty("found")
	private Boolean found = null;

	public OperSecurityAllType security(UserType security) {
		this.security = security;
		return this;
	}

	/**
	 * Get security
	 * 
	 * @return security
	 **/
	@ApiModelProperty(value = "")

	@Valid
	public UserType getSecurity() {
		return security;
	}

	public void setSecurity(UserType security) {
		this.security = security;
	}

	public OperSecurityAllType found(Boolean found) {
		this.found = found;
		return this;
	}

	/**
	 * Get found
	 * 
	 * @return found
	 **/
	@ApiModelProperty(value = "")

	public Boolean isFound() {
		return found;
	}

	public void setFound(Boolean found) {
		this.found = found;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OperSecurityAllType operSecurityAllType = (OperSecurityAllType) o;
		return Objects.equals(this.security, operSecurityAllType.security)
				&& Objects.equals(this.found, operSecurityAllType.found);
	}

	@Override
	public int hashCode() {
		return Objects.hash(security, found);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class OperSecurityAllType {\n");

		sb.append("    security: ").append(toIndentedString(security)).append("\n");
		sb.append("    found: ").append(toIndentedString(found)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
