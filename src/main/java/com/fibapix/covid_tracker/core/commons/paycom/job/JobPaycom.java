package com.fibapix.covid_tracker.core.commons.paycom.job;

public class JobPaycom {

	private String jobcode;
	private String title;
	private LocationPaycom location;
	private String deptcode;
	private String position_description;
	private String description;
	private String qualifications;
	private String socialMedia;
	private String url;

	public JobPaycom() {
		super();
	}

	public JobPaycom(String jobcode, String title, LocationPaycom location, String deptcode,
			String position_description, String description, String qualifications, String socialMedia, String url) {
		super();
		this.jobcode = jobcode;
		this.title = title;
		this.location = location;
		this.deptcode = deptcode;
		this.position_description = position_description;
		this.description = description;
		this.qualifications = qualifications;
		this.socialMedia = socialMedia;
		this.url = url;
	}

	public String getJobcode() {
		return jobcode;
	}

	public void setJobcode(String jobcode) {
		this.jobcode = jobcode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LocationPaycom getLocation() {
		return location;
	}

	public void setLocation(LocationPaycom location) {
		this.location = location;
	}

	public String getDeptcode() {
		return deptcode;
	}

	public void setDeptcode(String deptcode) {
		this.deptcode = deptcode;
	}

	public String getPosition_description() {
		return position_description;
	}

	public void setPosition_description(String position_description) {
		this.position_description = position_description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getQualifications() {
		return qualifications;
	}

	public void setQualifications(String qualifications) {
		this.qualifications = qualifications;
	}

	public String getSocialMedia() {
		return socialMedia;
	}

	public void setSocialMedia(String socialMedia) {
		this.socialMedia = socialMedia;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
