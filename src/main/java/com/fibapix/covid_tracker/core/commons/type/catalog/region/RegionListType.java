package com.fibapix.covid_tracker.core.commons.type.catalog.region;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fibapix.covid_tracker.core.commons.type.catalog.region.RegionType;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Collection of RegionType
 */
@ApiModel(description = "Collection of RegionType")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
public class RegionListType   {
  @JsonProperty("regions")
  @Valid
  private List<RegionType> regions = null;

  public RegionListType regions(List<RegionType> regions) {
    this.regions = regions;
    return this;
  }

  public RegionListType addRegionsItem(RegionType regionsItem) {
    if (this.regions == null) {
      this.regions = new ArrayList<RegionType>();
    }
    this.regions.add(regionsItem);
    return this;
  }

  /**
   * Get regions
   * @return regions
  **/
  @ApiModelProperty(value = "")
  @Valid
  public List<RegionType> getRegions() {
    return regions;
  }

  public void setRegions(List<RegionType> regions) {
    this.regions = regions;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RegionListType regionListType = (RegionListType) o;
    return Objects.equals(this.regions, regionListType.regions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(regions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RegionListType {\n");
    
    sb.append("    regions: ").append(toIndentedString(regions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
