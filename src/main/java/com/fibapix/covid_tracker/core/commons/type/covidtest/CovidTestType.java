package com.fibapix.covid_tracker.core.commons.type.covidtest;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fibapix.covid_tracker.core.commons.type.general.AuditType;
import com.fibapix.covid_tracker.core.commons.type.general.ImageType;

import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * CovidTestType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
public class CovidTestType extends AuditType  {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("recordCovidTestDate")
  private OffsetDateTime recordCovidTestDate = null;

  @JsonProperty("isPositive")
  private Boolean isPositive = null;

  @JsonProperty("photo")
  private ImageType photo = null;

  public CovidTestType id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public CovidTestType name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CovidTestType recordCovidTestDate(OffsetDateTime recordCovidTestDate) {
    this.recordCovidTestDate = recordCovidTestDate;
    return this;
  }

  /**
   * Get recordCovidTestDate
   * @return recordCovidTestDate
  **/
  @ApiModelProperty(value = "")

  @Valid
  public OffsetDateTime getRecordCovidTestDate() {
    return recordCovidTestDate;
  }

  public void setRecordCovidTestDate(OffsetDateTime recordCovidTestDate) {
    this.recordCovidTestDate = recordCovidTestDate;
  }

  public CovidTestType isPositive(Boolean isPositive) {
    this.isPositive = isPositive;
    return this;
  }

  /**
   * Get isPositive
   * @return isPositive
  **/
  @ApiModelProperty(value = "")

  public Boolean isIsPositive() {
    return isPositive;
  }

  public void setIsPositive(Boolean isPositive) {
    this.isPositive = isPositive;
  }

  public CovidTestType photo(ImageType photo) {
    this.photo = photo;
    return this;
  }

  /**
   * Get photo
   * @return photo
  **/
  @ApiModelProperty(value = "")

  @Valid
  public ImageType getPhoto() {
    return photo;
  }

  public void setPhoto(ImageType photo) {
    this.photo = photo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CovidTestType covidTestType = (CovidTestType) o;
    return Objects.equals(this.id, covidTestType.id) &&
        Objects.equals(this.name, covidTestType.name) &&
        Objects.equals(this.recordCovidTestDate, covidTestType.recordCovidTestDate) &&
        Objects.equals(this.isPositive, covidTestType.isPositive) &&
        Objects.equals(this.photo, covidTestType.photo) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, recordCovidTestDate, isPositive, photo, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CovidTestType {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    recordCovidTestDate: ").append(toIndentedString(recordCovidTestDate)).append("\n");
    sb.append("    isPositive: ").append(toIndentedString(isPositive)).append("\n");
    sb.append("    photo: ").append(toIndentedString(photo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
