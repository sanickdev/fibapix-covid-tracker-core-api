package com.fibapix.covid_tracker.core.commons.type.catalog.store;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fibapix.covid_tracker.core.commons.type.catalog.store.StoreType;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Collection of StoreType
 */
@ApiModel(description = "Collection of StoreType")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
public class StoreListType   {
  @JsonProperty("stores")
  @Valid
  private List<StoreType> stores = null;

  public StoreListType stores(List<StoreType> stores) {
    this.stores = stores;
    return this;
  }

  public StoreListType addStoresItem(StoreType storesItem) {
    if (this.stores == null) {
      this.stores = new ArrayList<StoreType>();
    }
    this.stores.add(storesItem);
    return this;
  }

  /**
   * Get stores
   * @return stores
  **/
  @ApiModelProperty(value = "")
  @Valid
  public List<StoreType> getStores() {
    return stores;
  }

  public void setStores(List<StoreType> stores) {
    this.stores = stores;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StoreListType storeListType = (StoreListType) o;
    return Objects.equals(this.stores, storeListType.stores);
  }

  @Override
  public int hashCode() {
    return Objects.hash(stores);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StoreListType {\n");
    
    sb.append("    stores: ").append(toIndentedString(stores)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
