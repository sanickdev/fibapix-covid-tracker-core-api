package com.fibapix.covid_tracker.core.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import javax.swing.JOptionPane;

public class UrlUtils {
	
	public static String readURL(String url) {
		String fileContents = "";
		String currentLine = "";
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
			fileContents = reader.readLine();
			while (currentLine != null) {
				currentLine = reader.readLine();
				fileContents += "\n" + currentLine;
			}
			reader.close();
			reader = null;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error Message", JOptionPane.OK_OPTION);
			e.printStackTrace();
		}
		return fileContents;
	}

}
