package com.fibapix.covid_tracker.core.converter.covidtest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.threeten.bp.DateTimeUtils;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneOffset;
import com.fibapix.covid_tracker.core.commons.domain.covidtest.TCovidTest;
import com.fibapix.covid_tracker.core.commons.domain.general.TImage;
import com.fibapix.covid_tracker.core.commons.type.covidtest.CovidTestType;
import com.fibapix.covid_tracker.core.commons.type.general.ImageType;

public class CovidTestConverter {

	public static TCovidTest convertTypeToEntity(CovidTestType covidTest) {
		TCovidTest tCovidTest = new TCovidTest();
		if(covidTest != null) {
			if(covidTest.getId() != null) {
				tCovidTest.setId(covidTest.getId());
			}
			if(covidTest.getName() != null) {
				tCovidTest.setName(covidTest.getName());
			}
			if(covidTest.getPhoto() != null) {
				TImage tImage = new TImage();
				if(covidTest.getPhoto().getId() != null) {
					tImage.setId(covidTest.getPhoto().getId());
				}
				if(covidTest.getPhoto().getBase64() != null) {
					tImage.setBase64(covidTest.getPhoto().getBase64());
				}
				tCovidTest.setPhoto(tImage);
			}
			if(covidTest.getRecordCovidTestDate() != null) {
				tCovidTest.setRecordCovidTestDate(DateTimeUtils.toDate(covidTest.getRecordCovidTestDate().toInstant()));
			}
			//---------
			if(covidTest.getRecordDate() == null) {
				tCovidTest.setRecordDate(new Date());
			}else {
				tCovidTest.setUpdateDate(new Date());
			}
			if(covidTest.getRecordUsername() == null) {
				tCovidTest.setRecordUsername("Admin");
			}else {
				tCovidTest.setUpdateUsername("Admin");
			}
			tCovidTest.setStatus(covidTest.isStatus());
		}
		return tCovidTest;
	}
	
	public static List<CovidTestType> convertListEntityToType(List<TCovidTest> lstTCovidTest) {
		List<CovidTestType> lstCovidTestType = new ArrayList<CovidTestType>();
		if(lstTCovidTest.size() > 0 ) {
			for(TCovidTest tReview: lstTCovidTest){
				lstCovidTestType.add(CovidTestConverter.convertEntityToType(tReview));
			}
		}
		return lstCovidTestType;
	}
	
	public static CovidTestType convertEntityToType(TCovidTest tCovidTest) {
		CovidTestType covidTestType = new CovidTestType();
		if(tCovidTest != null) {
			if(tCovidTest.getId() != null) {
				covidTestType.setId(tCovidTest.getId());
			}
			if(tCovidTest.getName() != null) {
				covidTestType.setName(tCovidTest.getName());
			}
			if(tCovidTest.getPhoto() != null) {
				ImageType imageType = new ImageType();
				if(tCovidTest.getPhoto().getId() != null) {
					imageType.setId(tCovidTest.getPhoto().getId());
				}
				if(tCovidTest.getPhoto().getBase64() != null) {
					imageType.setBase64(tCovidTest.getPhoto().getBase64());
				}
				covidTestType.setPhoto(imageType);
			}
			if(tCovidTest.getRecordCovidTestDate() != null) {
				OffsetDateTime odt = DateTimeUtils.toInstant(tCovidTest.getRecordCovidTestDate()).atOffset(ZoneOffset.UTC);
				covidTestType.setRecordCovidTestDate(odt);
			}
			covidTestType.setStatus(tCovidTest.isStatus());
		}
		return covidTestType;
	}

}
