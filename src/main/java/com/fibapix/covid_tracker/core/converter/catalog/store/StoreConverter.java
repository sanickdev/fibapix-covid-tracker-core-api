package com.fibapix.covid_tracker.core.converter.catalog.store;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.fibapix.covid_tracker.core.commons.domain.catalog.store.TStore;
import com.fibapix.covid_tracker.core.commons.type.catalog.store.StoreType;

public class StoreConverter {
	
	public static TStore convertTypeToEntity(StoreType storeType) {
		TStore tStore = new TStore();
		if(storeType != null) {
			if(storeType.getId() != null) {
				tStore.setId(storeType.getId());
			}
			if(storeType.getName() != null) {
				tStore.setName(storeType.getName());
			}
			//---------
			if(storeType.getRecordDate() == null) {
				tStore.setRecordDate(new Date());
			}else {
				tStore.setUpdateDate(new Date());
			}
			if(storeType.getRecordUsername() == null) {
				tStore.setRecordUsername("Admin");
			}else {
				tStore.setUpdateUsername("Admin");
			}
			tStore.setStatus(storeType.isStatus());
		}
		return tStore;
	}
	
	public static List<StoreType> convertListEntityToType(List<TStore> lstTStore) {
		List<StoreType> lstStoreType = new ArrayList<StoreType>();
		if(lstTStore.size() > 0) {
			for(TStore tStore: lstTStore) {
				lstStoreType.add(StoreConverter.convertEntityToType(tStore));
			}
		}
		return lstStoreType;
	}
	
	public static StoreType convertEntityToType(TStore tStore) {
		StoreType storeType = new StoreType();
		if(tStore != null) {
			if(tStore.getId() != null) {
				storeType.setId(storeType.getId());
			}
			if(tStore.getName() != null) {
				storeType.setName(storeType.getName());
			}
			storeType.setStatus(tStore.isStatus());
		}
		return storeType;
	}

}
