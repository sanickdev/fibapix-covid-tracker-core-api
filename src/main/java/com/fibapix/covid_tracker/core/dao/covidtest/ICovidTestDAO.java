package com.fibapix.covid_tracker.core.dao.covidtest;

import java.util.List;
import com.fibapix.covid_tracker.core.commons.domain.covidtest.TCovidTest;

public interface ICovidTestDAO {
	
	public List<TCovidTest> getAll();
	public void save(TCovidTest covidTest);
	public void update(TCovidTest covidTest);
	public TCovidTest getCovidTestById(Long id);
	public void disable(Long id);

}
