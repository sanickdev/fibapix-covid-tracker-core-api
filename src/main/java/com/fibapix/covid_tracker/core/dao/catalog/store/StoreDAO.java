package com.fibapix.covid_tracker.core.dao.catalog.store;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fibapix.covid_tracker.core.commons.domain.catalog.store.TStore;

@Repository
public class StoreDAO implements IStoreDAO {

	private final SessionFactory sessionFactory;

	@Autowired
	public StoreDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<TStore> getAll() {
		@SuppressWarnings({ "deprecation", "unchecked" })
		List<TStore> list = sessionFactory.getCurrentSession()
				.createSQLQuery("SELECT * FROM " + TStore.class.getSimpleName())
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
		return list != null ? list : new ArrayList<>();
	}

	@Override
	public void save(TStore store) {
		sessionFactory.getCurrentSession().save(store);
	}

	@Override
	public void update(TStore store) {
		sessionFactory.getCurrentSession().save(store);
	}

	@Override
	public TStore getStoreById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void disable(Long id) {
		// TODO Auto-generated method stub

	}

}
