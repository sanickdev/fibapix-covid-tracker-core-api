package com.fibapix.covid_tracker.core.dao.security;

import com.fibapix.covid_tracker.core.commons.type.security.OperSecurityAllType;
import com.fibapix.covid_tracker.core.commons.type.security.UserType;

public interface IPaycomSecurityDAO {
	
	public OperSecurityAllType login(UserType userType);

}
