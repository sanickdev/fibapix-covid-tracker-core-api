package com.fibapix.covid_tracker.core.dao.covidtest;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fibapix.covid_tracker.core.commons.domain.covidtest.TCovidTest;

@Repository
public class CovidTestDAO implements ICovidTestDAO {

	private final SessionFactory sessionFactory;

	@Autowired
	public CovidTestDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<TCovidTest> getAll() {
		List<TCovidTest> list = sessionFactory.getCurrentSession()
				.createSQLQuery("SELECT * FROM " + TCovidTest.class.getSimpleName())
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
		return list != null ? list : new ArrayList<>();
	}

	@Override
	public void save(TCovidTest covidTest) {
		sessionFactory.getCurrentSession().save(covidTest);
	}

	@Override
	public void update(TCovidTest covidTest) {
		sessionFactory.getCurrentSession().update(covidTest);
	}

	@Override
	public TCovidTest getCovidTestById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void disable(Long id) {
		//sessionFactory.getCurrentSession()
		
	}


}
