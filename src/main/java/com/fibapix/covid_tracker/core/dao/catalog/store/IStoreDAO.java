package com.fibapix.covid_tracker.core.dao.catalog.store;

import java.util.List;
import com.fibapix.covid_tracker.core.commons.domain.catalog.store.TStore;

public interface IStoreDAO {
	
	public List<TStore> getAll();
	public void save(TStore store);
	public void update(TStore store);
	public TStore getStoreById(Long id);
	public void disable(Long id);

}
