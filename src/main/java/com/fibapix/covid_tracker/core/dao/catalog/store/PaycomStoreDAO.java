package com.fibapix.covid_tracker.core.dao.catalog.store;

import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import com.fibapix.covid_tracker.core.commons.paycom.job.JobPaycom;
import com.fibapix.covid_tracker.core.util.UrlUtils;
import com.google.gson.Gson;

@Repository
public class PaycomStoreDAO implements IPaycomStoreDAO {

	@Value("${com.fibapix.covidtracker.paycom.url}")
	private String urlPaycom;

	@Value("${com.fibapix.covidtracker.paycom.clientkey}")
	private String clientkeyPaycom;

	@Override
	public List<?> getJobs() {
		this.extractJobFromPaycom();
		return null;
	}

	private void extractJobFromPaycom() {

		Document doc = Jsoup.parse(UrlUtils.readURL(urlPaycom + "/jobs?clientkey=" + clientkeyPaycom));
		Elements scriptElements = doc.select("script");
		Element script = scriptElements.get(52);
		List<JobPaycom> lstJob = getJobValue(script.html());
		System.out.println(lstJob);
	}

	private List<JobPaycom> getJobValue(String data) {
		List<String> lstValues = getListOfExtractedObject(data);
		List<JobPaycom> lstJob = new ArrayList<JobPaycom>();
		for (String value : lstValues) {
			Gson gson = new Gson();
			JobPaycom job = gson.fromJson("{" + value + "}", JobPaycom.class);
			lstJob.add(job);
		}
		return lstJob;
	}

	private List<String> getListOfExtractedObject(String data) {
		data = data.replaceAll("var jobs = ", "");
		data = data.replaceAll(";", "");
		String[] allValues = data.split("jobcode\":");
		List<String> lstValues = new ArrayList<String>();
		for (int index = 0; index < allValues.length; index++) {
			if (!allValues[index].contains("[{")) {
				if (allValues.length == (index + 1)) {
					lstValues.add("\"jobcode\":" + allValues[index].substring(0, allValues[index].length() - 2));
				} else {
					lstValues.add("\"jobcode\":" + allValues[index].substring(0, allValues[index].length() - 4));
				}
			}
		}
		return lstValues;

	}

}

/*
 * 
 * 
 * // Pattern pattern = Pattern.compile("(?<=jobs\\s=\\s\").*?(?=\")"); Pattern
 * pattern = Pattern.compile("(?<=jobs)"); Matcher m =
 * pattern.matcher(script.html()); // you have to use html here and NOT text!
 * Text will drop the 'key' // part List<JobPaycom> lstJob =
 * getJobValue(script.html());
 * 
 * while (m.find()) { System.out.println(m.group()); // the whole key ('key =
 * value') System.out.println(m.group(1)); // value only }
 * 
 * 
 * Regex curly braces pattern: "\\{(.*?)\\}"
 * 
 * 
 */
