package com.fibapix.covid_tracker.core.api;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-14T23:30:23.331-06:00[America/Mexico_City]")
public class ApiException extends Exception{
	
	private static final long serialVersionUID = 5240759324741794120L;
	private int code;
    public ApiException (int code, String msg) {
        super(msg);
        this.code = code;
    }
}
