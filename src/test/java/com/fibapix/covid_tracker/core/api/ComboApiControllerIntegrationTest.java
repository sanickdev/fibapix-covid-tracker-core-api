package com.fibapix.covid_tracker.core.api;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fibapix.covid_tracker.core.commons.type.combo.OperComboListAllType;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ComboApiControllerIntegrationTest {

    @Autowired
    private ComboApi api;

    @Test
    public void getComboTest() throws Exception {
        String option = "option_example";
        ResponseEntity<OperComboListAllType> responseEntity = api.getCombo(option);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
