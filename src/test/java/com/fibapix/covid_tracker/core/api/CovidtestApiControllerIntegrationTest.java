package com.fibapix.covid_tracker.core.api;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fibapix.covid_tracker.core.commons.type.covidtest.OperCovidTestAllType;
import com.fibapix.covid_tracker.core.commons.type.covidtest.OperCovidTestListAllType;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CovidtestApiControllerIntegrationTest {

    @Autowired
    private CovidtestApi api;

    @Test
    public void getAllTest() throws Exception {
        Long id = 789L;
        ResponseEntity<OperCovidTestListAllType> responseEntity = api.getAll(id);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void saveTest() throws Exception {
        OperCovidTestAllType body = new OperCovidTestAllType();
        ResponseEntity<OperCovidTestAllType> responseEntity = api.save(body);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void updateTest() throws Exception {
        OperCovidTestAllType body = new OperCovidTestAllType();
        ResponseEntity<OperCovidTestAllType> responseEntity = api.update(body);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
