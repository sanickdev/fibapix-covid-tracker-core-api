package com.fibapix.covid_tracker.core.api;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fibapix.covid_tracker.core.commons.type.catalog.region.OperRegionAllType;
import com.fibapix.covid_tracker.core.commons.type.catalog.region.OperRegionListAllType;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegionApiControllerIntegrationTest {

    @Autowired
    private RegionApi api;

    @Test
    public void disableTest() throws Exception {
        Long id = 789L;
        ResponseEntity<Void> responseEntity = api.disable(id);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void getAllTest() throws Exception {
        Long id = 789L;
        String name = "name_example";
        ResponseEntity<OperRegionListAllType> responseEntity = api.getAll(id, name);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void getRegionByIdTest() throws Exception {
        Long id = 789L;
        ResponseEntity<OperRegionAllType> responseEntity = api.getRegionById(id);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void saveTest() throws Exception {
        OperRegionAllType body = new OperRegionAllType();
        ResponseEntity<OperRegionAllType> responseEntity = api.save(body);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void updateTest() throws Exception {
        OperRegionAllType body = new OperRegionAllType();
        ResponseEntity<OperRegionAllType> responseEntity = api.update(body);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
